# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from flask import Blueprint, jsonify, request, Response
from jsonschema import validate

from erudite.db import get_db
from erudite.error import NotFoundException
from erudite.auth import auth

bp = Blueprint('author', __name__, url_prefix='/authors')
schema = {
    "type": "object",
    "properties": {
        "name": {
            "type": "string",
        }
    },
    "required": [
        "name"
    ]
}

class AuthorNotFoundException(NotFoundException):
    def __init__(self, idx):
        super().__init__("author", idx)

def search_books_by_id_author(id_author: int):
    """
    Search books by author

    Parameters
    ----------
    id_author : int
        ID of author

    Returns
    -------
    dict
        dictionary representing found books
    """

    db = get_db()
    sql = """
    SELECT DISTINCT id_book, title, isbn, pages, year, id_publisher, description, cover
    FROM book JOIN book_author USING (id_book) WHERE id_author = %s
    """
    db.execute(sql, (id_author,))
    books = db.fetchall()
    for book in books:
        sql = """
        SELECT a.id_author, a.name FROM author a
        JOIN book_author ba USING (id_author)
        WHERE ba.id_book = %s
        """
        db.execute(sql, (book["id_book"],))
        book["authors"] = db.fetchall()
    return books

def add_author_to_db(author: dict):
    validate(instance=author, schema=schema)
    db = get_db()
    sql = """
    INSERT INTO author(name)
    VALUES (%(name)s)
    """
    db.execute(sql, author)

def search_authors_by_name(name: str):
    db = get_db()
    sql = """
    SELECT id_author, name FROM author
    WHERE name ilike '%%' || %s || '%%'
    """
    db.execute(sql, (name,))
    return db.fetchall()


def fetch_all_authors():
    db = get_db()
    sql = """
    SELECT id_author, name FROM author
    """
    db.execute(sql)
    return db.fetchall()

@bp.get("/<int:id_author>/get_books")
def get_books_for_given_author_endpoint(id_author):
    """
    Get endpoint for finding books by author

    Parameters
    ----------
    id_author : int
        ID of author

    Returns
    -------
    json
        json representing found books
    """

    books = dict()
    books = search_books_by_id_author(id_author)
    return jsonify(books)

def update_author(id_author: int, name: str):
    """
    Update author in database

    Parameters
    ----------
    id_author : int
        ID of author
    name : str
        Name of author

    """

    db = get_db()
    sql = """
    UPDATE author
    SET name = %s
    WHERE id_author = %s
    """
    db.execute(sql, (name, id_author))


def remove_author(id_author: int):
    """
    Remove tag from database

    Parameters
    ----------
    id_author : int
        ID of author

    """

    db = get_db()
    sql = """
    DELETE FROM book_author
    WHERE id_author = %s
    """
    db.execute(sql, (id_author,))
    sql = """
    DELETE FROM author
    WHERE id_author = %s
    """
    db.execute(sql, (id_author,))

@bp.post("")
@auth.login_required(role = 'librarian')
def post_author_endpoint():
    author = request.get_json()
    add_author_to_db(author)
    return '', 204

@bp.put("/<int:id_author>")
@auth.login_required(role="librarian")
def update_author_endpoint(id_author: int):
    data = request.get_json()
    update_author(id_author, data["name"])
    return '', 204

@bp.delete("/<int:id_author>")
@auth.login_required(role="librarian")
def remove_author_endpoint(id_author: int):
    remove_author(id_author)
    return '', 204


@bp.get("")
def get_all_authors_endpoint():
    authors = fetch_all_authors()
    return jsonify(authors)