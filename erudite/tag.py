# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from flask import Blueprint, jsonify, request, Response
from jsonschema import validate

from erudite.db import get_db
from erudite.error import NotFoundException
from erudite.auth import auth

bp = Blueprint('tag', __name__, url_prefix='/tags')
schema = {
    "type": "object",
    "properties": {
        "name": {
            "type": "string"
        },
        "color": {
            "type": "string",
            "format": "^#[A-Fa-f0-9]{6}$"
        }
    },
    "required": [
        "name", "color"
    ]
}

class TagNotFoundException(NotFoundException):
    def __init__(self, idx):
        super().__init__("tag", idx)

def get_all_tags():
    """
    Get all tags from database

    Returns
    -------
    dict
        dictionary representing all tags
    """

    db = get_db()
    sql = """
    SELECT id_tag, name, color
    FROM tag
    """
    db.execute(sql)
    tags = db.fetchall()
    return tags

def add_tag_to_db(tag: dict):
    validate(instance=tag, schema=schema)
    db = get_db()
    sql = """
    INSERT INTO tag(name, color)
    VALUES (%(name)s, %(color)s)
    """
    db.execute(sql, tag)

@bp.get("")
def get_all_tags_endpoint():
    """
    Get endpoint for finding all tags

    Returns
    -------
    json
        json representing found tags
    """
    tags = dict()
    tags = get_all_tags()
    return jsonify(tags)

@bp.post("")
@auth.login_required(role = 'librarian')
def post_tag_endpoint():
    tag = request.get_json()
    add_tag_to_db(tag)
    return '', 204

def update_tag_in_database(id_tag: int, name: str, color: str):
    """
    Update tag in database

    Parameters
    ----------
    id_tag : int
        ID of tag
    name : str
        Name of tag
    color : str
        Color of tag

    """

    db = get_db()
    sql = """
    UPDATE tag
    SET name = %s, color = %s
    WHERE id_tag = %s
    """
    db.execute(sql, (name, color, id_tag))

def remove_tag_from_database(id_tag: int):
    """
    Remove tag from database

    Parameters
    ----------
    id_tag : int
        ID of tag

    """
    
    db = get_db()
    sql = """
    DELETE FROM book_tag WHERE id_tag = %s
    """
    db.execute(sql, (id_tag,))
    sql = """
    DELETE FROM tag WHERE id_tag = %s
    """
    db.execute(sql, (id_tag,))


@bp.delete("/<int:id_tag>")
@auth.login_required(role="librarian")
def remove_tag_endpoint(id_tag: int):
    remove_tag_from_database(id_tag)
    return '', 204

@bp.put("/<int:id_tag>")
@auth.login_required(role="librarian")
def update_tag_endpoint(id_tag: int):
    data = request.get_json()
    validate(data, schema)
    update_tag_in_database(id_tag, data["name"], data["color"])
    return '', 204



