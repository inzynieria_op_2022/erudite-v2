erudite package
===============

Submodules
----------

erudite.auth module
-------------------

.. automodule:: erudite.auth
   :members:
   :undoc-members:
   :show-inheritance:

erudite.author module
---------------------

.. automodule:: erudite.author
   :members:
   :undoc-members:
   :show-inheritance:

erudite.book module
-------------------

.. automodule:: erudite.book
   :members:
   :undoc-members:
   :show-inheritance:

erudite.catalog module
----------------------

.. automodule:: erudite.catalog
   :members:
   :undoc-members:
   :show-inheritance:

erudite.db module
-----------------

.. automodule:: erudite.db
   :members:
   :undoc-members:
   :show-inheritance:

erudite.error module
--------------------

.. automodule:: erudite.error
   :members:
   :undoc-members:
   :show-inheritance:

erudite.googlebooks module
--------------------------

.. automodule:: erudite.googlebooks
   :members:
   :undoc-members:
   :show-inheritance:

erudite.loan module
-------------------

.. automodule:: erudite.loan
   :members:
   :undoc-members:
   :show-inheritance:

erudite.parse module
--------------------

.. automodule:: erudite.parse
   :members:
   :undoc-members:
   :show-inheritance:

erudite.publisher module
------------------------

.. automodule:: erudite.publisher
   :members:
   :undoc-members:
   :show-inheritance:

erudite.reservations module
---------------------------

.. automodule:: erudite.reservations
   :members:
   :undoc-members:
   :show-inheritance:

erudite.review module
---------------------

.. automodule:: erudite.review
   :members:
   :undoc-members:
   :show-inheritance:

erudite.tag module
------------------

.. automodule:: erudite.tag
   :members:
   :undoc-members:
   :show-inheritance:

erudite.upload module
---------------------

.. automodule:: erudite.upload
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: erudite
   :members:
   :undoc-members:
   :show-inheritance:
