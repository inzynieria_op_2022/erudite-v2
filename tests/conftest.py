# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

import pytest
from erudite import create_app
from erudite.db import init_db, get_db

import os

with open(os.path.join(os.path.dirname(__file__), 'data.sql'), 'rb') as f:
    _data_sql = f.read().decode('utf8')

@pytest.fixture
def app():
    app = create_app()
    app.config.update({
        "TESTING": True
    })

    with app.app_context():
        init_db()
        get_db().execute(_data_sql)

    yield app

@pytest.fixture
def client(app):
    return app.test_client()
