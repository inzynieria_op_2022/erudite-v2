# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

def test_getting_review(client):
    response = client.get("/reviews/1")
    assert response.status_code == 200
    review = response.get_json()
    assert review["id_review"] == 1
    assert review["id_user"] == 2
    assert review["id_book"] == 6
    assert review["content"] == "Fajne. Bardzo fajne."


def test_getting_reviews_for_book(client):
    request_data = {"id_book": 6}
    response = client.get("/reviews/get_for_books", query_string=request_data)
    assert response.status_code == 200
    reviews = response.get_json()
    for review in reviews:
        assert review["id_book"] == 6
        assert len(review) == 4

def test_getting_reviews_for_user(client):
    request_data = {"id_user": 1}
    response = client.get("/reviews/get_for_users", query_string=request_data)
    assert response.status_code == 200
    reviews = response.get_json()
    for review in reviews:
        assert review["id_user"] == 1
        assert len(review) == 4
