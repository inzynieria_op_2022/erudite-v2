# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from tests.auth import user, librarian
import os


def test_getting_book(client):
    response = client.get('/books/1')
    assert response.status_code == 200
    book = response.get_json()
    assert book["title"] == "Krew elfów"
    assert "isbn" in book
    assert book["pages"] == 295
    assert book["year"] == 1994
    assert "cover" in book
    assert "description" in book
    assert "publisher" in book
    assert "tags" in book

def test_getting_books(client):
    response = client.get('/books')
    assert response.status_code == 200
    books = response.get_json()
    for book in books:
        assert "id_book" in book
        assert "cover" in book
        assert "title" in book
        assert "authors" in book
        assert "tags" in book
        assert "publisher" in book
        assert "year" in book
        assert "description" in book

def test_searching_with_title(client):
    response = client.get('/books/search?title=potter')
    assert response.status_code == 200
    books = response.get_json()
    book = books[0]
    assert "id_book" in book
    assert "cover" in book
    assert book["title"] == "Harry Potter i wiezień Azkabanu"
    authors = book["authors"]
    assert authors[0]["name"] == "J.K. Rowling"

def test_searching_with_author(client):
    response = client.get('/books/search?author=sapkowski')
    assert response.status_code == 200
    books = response.get_json()
    book = books[0]
    assert "id_book" in book
    assert "cover" in book
    assert book["title"] == "Krew elfów"
    authors = book["authors"]
    assert authors[0]["name"] == "Andrzej Sapkowski"

def test_searching_with_isbn(client):
    response = client.get('/books/search?isbn=9788366969735')
    assert response.status_code == 200
    books = response.get_json()
    book = books[0]
    assert "id_book" in book
    assert "cover" in book
    assert book["title"] == "Dziady"
    authors = book["authors"]
    assert authors[0]["name"] == "Adam Mickiewicz"

def test_searching_by_string(client):
    response = client.get('/books/search?s=lalk')
    assert response.status_code == 200
    books = response.get_json()
    book = books[0]
    assert "id_book" in book
    assert "cover" in book
    assert book["title"] == "Lalka"
    authors = book["authors"]
    assert authors[0]["name"] == "Bolesław Prus"


def test_adding_review(client):
    data = {
        "content": "This book is awesome!"
    }
    response = client.post('/books/1/review', json=data, auth=user)
    assert response.status_code == 204
    assert response.get_data() == b''

def test_adding_review_to_non_existing_book(client):
    data = {
        "content": "This book is awesome!"
    }
    response = client.post('/books/100/review', json=data, auth=user)
    assert response.status_code == 404

def test_adding_book(client):
    data = {
        "title": "Test book",
        "authors": [1, 2],
        "isbn": "1234567890",
        "pages": 100,
        "year": 2020,
        "id_publisher": 1,
        "description": "This is a test book",
        "tags": [1, 2]
    }
    response = client.post('/books', json=data, auth=librarian)
    assert response.status_code == 200


def test_adding_cover(client):
    image = open(os.path.join(os.path.dirname(__file__), 'files', 'test.jpg'), 'rb')
    data = {"file": (image, 'test.jpg')}
    response = client.post('/books/1/upload_cover', data=data, buffered=True, content_type="multipart/form-data", auth=librarian)
    image.close()
    assert response.status_code == 204

def test_adding_cover_to_non_existing_book(client):
    image = open(os.path.join(os.path.dirname(__file__), 'files', 'test.jpg'), 'rb')
    data = {"file": (image, 'test.jpg')}
    response = client.post('/books/100/upload_cover', data=data, buffered=True, content_type="multipart/form-data", auth=librarian)
    image.close()
    assert response.status_code == 404

def test_add_with_isbn_endpoint(client):
    response = client.post('/books/add_with_isbn?isbn=9781503280786', auth=librarian)
    assert response.status_code == 200
    book = response.get_json()

    r_image = client.get(book['cover'])
    assert r_image.status_code == 200


def test_update_book(client):
    data = {
        "title": "Test book",
        "authors": [1, 2],
        "isbn": "1234567890",
        "pages": 100,
        "year": 2020,
        "id_publisher": 1,
        "description": "This is a test book",
        "tags": [1, 2]
    }
    response = client.put('/books/1', json=data, auth=librarian)
    assert response.status_code == 200
    

