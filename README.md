# Erudite

**Erudite** is a Library Management System backend written in Python using
Flask web framework. 

# Dependencies
Running Erudite requires following dependencies:
- Python 3.7
- Python libraries listed in requirements.txt file
- Running instance of PostgreSQL database

## Running in development environment
In order to run program
1. Clone it

``` sh
git clone 'https://gitlab.com/inzynieria_op_2022/erudite-v2'
```

2. Create and activate virtual environment

``` sh
python3 -m venv venv
. venv/bin/activate
```

3. Install requirements
``` sh
pip3 install -r requirements.txt
```

4. Create .env file with configuration

``` sh
ERUDITE_DATABASE=<postgres_conn_info>
```
<postgres_conn_info> is a connection string conforming to [official PostgreSQL
documentation](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING)

5. Initialize database

``` sh
flask --app erudite init-db
```

6. Run flask application
``` sh
flask --app erudite run --debugger
```

## Unit testing
In order to run unit tests follow instructions from
[Installation](##Installation) up to step 4 and then

7. Run pytest

``` sh
python -m pytest -vv
```

Alternatively, if you want to also get test coverage information you can run

``` sh
coverage run -m pytest
coverage report
```

## Generating documentation

8. Generate documentation
``` sh
sphinx-apidoc -o docs erudite/
cd docs
make html
```


## Deploying

In order to deploy Erudite to production you have to choose one of many
WSGI production server. See [Flask documentation](https://flask.palletsprojects.com/en/2.2.x/deploying/) for more
information.

## Configuration
All configuration is done using .env file and environment variables. All
possible key-value pairs are listed in [Flask documentation](https://flask.palletsprojects.com/en/2.2.x/config/). Keep in
mind that you have to prefix them with ERUDITE_ though.

For web server related configurations check documentation of WSGI production server that you have chosen in [deployment](##Deploying).
